Supported Vehicles
==================

The full range of supported vehicles is not precisely known. Put simply, the more people try the software with their vehicle and find issues, the more things will get fixed! The development team is very active, and keen to fix things quickly.

Vehicles/electronic combinations tested in depth:

- **Citroen C5 X7 (2008 on)** - BSI04EV / COMBINE / EDC16CP39 / EDC17CP11 / etc
- **Peugeot 208 (2010 - 2018)** - BSI2010ECO 
- **Peugeot 3008 Mk1 (2008-2016)** - VTH
- **Peuygeot 307 (T6)** - BSI04 / COMBINE / SID803 / etc

However, in general, any vehicle based on the AEE2004 and AEE2010 architectures should *mostly* work. This includes most vehicles built by PSA between 2004 and 2020.

- **Peugeot** - 107/108/207/208/307/308/3008/5008/2008/407/508
- **Citroen** 
- **DS** - most models
- **Vauxhall** - 2018-on released vehicles (Mokka/Grandland/Corsa etc)

In addition, the following vehicles are likely to work in future, once tested, as they are badge engineered versions of Peugeot vehicles (or Peugeot sold badge engineered versions of their own):

- Mitsubishi Outlander I
- Toyota Aygo
- Various Fiat, Toyota and Vauxhall vans (will require additional cabling)
