Telecoding
==========

Public service announcement
---------------------------
**Currently** telecoding functionality should be considered experimental. There is no risk involved in *reading* the configuration, but attempting to *write* it back could cause damage. Communication with ECUs in the CAN2004 generation, particularly simple ones and the BSI, should be fine. Engine ECU's are often more complex, and are likely to fail.

Overview
--------
The screen allows modification of the configuration of an ECU, for example enabling/disabling a CD changer port or for installation of a towbar electrics package. The list of options available depends on the vehicle, the ECU and the software's experience level (see Settings). At the top are buttons for Importing/Exporting configuration and Writing changes to the ECU.

The list displays a Dropdown box for telecoding values with a series of options, a simple switch for on/off values, or a textbox when the value can be entered by the user (currently disabled for safety reasons).

It is not recommended to change any configuration when you are not 100% sure of the result. To enable some functions you will need to change more than one setting, and so it is recommended to seek expert advice from the community before attempting to do so.
