Procedures
==========

This tab covers "special functions" for a particular ECU. Examples include programming keys to the BSI, or depressurising hydraulic suspension. These functions are a major **"work-in-progress"**!

Several functions have been decoded, but re-implementing them in the database is also a task in itself and the utility to do so is not written yet.
