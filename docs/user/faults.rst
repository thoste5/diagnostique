Fault Reading & Clearing
========================

This is the default screen, and shows any currently stored faults in the selected ECU. Clicking on the plus (+) symbol will cause it to expand and display any information associated with the fault, such as "Characterisations" or a freeze frame of data values stored by the ECU when the fault occurred.

Note that some faults will not clear until the fault is rectified.

Freeze frame and characterisation data is not always available depending on fault code and ECU. Sometimes the freeze frame table will display confusing or nonsensical values. It is not yet known why this happens, but it occurs on official kits too.

**IMPORTANT**: A few ECU's (notably the AM6 auto gearbox) will flag certain faults in memory as part of being diagnosed. These will clear automatically once the car ignition is switched off and left for approx 90 seconds. There is nothing to be done about this, it is a bug in the ECU code from the factory.

