About
=====

Disclaimer
----------
- ABSOLUTELY NO WARRANTY IS GIVEN OR IMPLIED. YOU USE THIS SOFTWARE AT YOUR OWN RISK
- This software has the ability to change settings which may make your vehicle unsafe to drive, if used incorrectly. NEVER change a setting unless you know that it is safe and legal to do so
- The project is currently in the **ALPHA** stage, which means you should not use **telecoding** or **procedures** on a real car unless you have full knowledge of the consequences and/or the ability to repair any misconfiguration or damage that may result


Licencing
---------
You are granted the freedom to use the software in the form it is provided to you for your own private or commercial purposes. You are granted the freedom to see the code and modify it for use in your own private projects. You are free to modify the software to suit your own private purposes or private projects, and the developers will always welcome bugfixes, new features or any other contribution you might wish to make. You may not re-package or otherwise distribute and you absolutely *must not attempt to sell* the software or other project code or information. If you would like to release or distribute software based off or including parts of our application and/or libraries please get in touch. Permission is not blindly granted, but we do approve on a case-by-case basis. This is to avoid dodgy software damaging people's vehicles being linked to us.

THIS INCLUDES SELLING CD's, DVD's, USB DRIVES OR ANY OTHER KIND OF PACKAGE. If you sell diagnostic hardware that's compatible with this software, by all means link to our main site but DO NOT distribute our releases by yourself. We fix and upgrade things rapidly, and having to attempt troubleshooting because of old versions just slows that down.


The Project
-----------
This software and associated documentation has been created by and for enthusiasts in the car community, using information gleaned from publicly available sources as well as reverse engineering, analysis and experimentation. We are firm believers in the principles of right-to-repair, but with that comes a responsibility. Whenever you do anything to your car, do not forget that it is a two-tonne lump of metal that could cause serious injury to someone (including yourself) if you get something wrong. 


Credits
-------
Diagnostique uses the excellent cross-platform, **Kivy** Python UI framework

- Associated tools use the **tkinter** library
- **DBeaver Community** - an *excellent* database browser, viewer and editor. This project would simply not have been possible without it.
- **vlud** - the arduino-psa-diag sketch and various associated info was of great help in the early stages of development
- **ptx, morcibacsi, ath** - and others who have made contributions large and small, but sometimes it's the smallest things that unlock a huge discovery!
