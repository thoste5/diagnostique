User Guide
==========

**IMPORTANT**: Due to the incredible complexity of the system, not all ECUs have been tested, nor could they be (there are more than 34,000 variations). As a result, you will occasionally run into errors or "exceptions" to known rules. The system will always attempt to fail "safe", by safely aborting any ECU action in progress. Error logs are detailed and will allow the developers to identify (and hopefully fix) the problem, so PLEASE send us the logfile/a description of what went wrong!

Getting Started
---------------
1. Download and install the software. It should include all necessary dependencies, but if you run into a problem take a look at the FAQ
2. Connect your chosen hardware
3. Start the software and click/tap "Configure" in the top right, then choose the type of adapter and then its COM port (if you're using EvolutionXS, you don't need to choose the COM port). You can also adjust any other settings to suit your preferences, such as Language (requires restarting software).


First Run
---------
1. Start the software from the shortcut. You are presented with three choices.
